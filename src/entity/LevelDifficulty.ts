import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Difficulty } from "./Difficulty";




@Entity()

export class LevelDifficulty{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    answers: string;

    @Column({default: 1})
    coef: number;

    @ManyToOne(()=> Difficulty, difficulty => difficulty.levelDifficulty)
    difficulty: Difficulty
}