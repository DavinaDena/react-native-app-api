import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { LevelDifficulty } from "./LevelDifficulty";




@Entity()

export class Difficulty{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    question: string;

    @OneToMany(()=> LevelDifficulty, level => level.difficulty)
    levelDifficulty: LevelDifficulty[]
}