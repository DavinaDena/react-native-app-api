import { Router } from "express";
import { getRepository } from "typeorm";
import { LevelDifficulty } from "../entity/LevelDifficulty";

export const levelDiffiController = Router()

//Get answers by ID
levelDiffiController.get('/:id', async (req, res)=>{
    try {
        let answers = await getRepository(LevelDifficulty).findOne(req.body.id, {relations: ['difficulty']})
        if(answers){
            return res.json(answers)
        }return res.status(404).json({message: 'no answers found'})
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// Post new answers
levelDiffiController.post('/', async (req, res) => {
    try {

        let answer = new LevelDifficulty();
        Object.assign(answer, req.body)
        await getRepository(LevelDifficulty).save(answer)

        res.status(201).json(answer)
    } catch (error) {
        console.log(error);
        res.status(400).json(error)
    }
})


//Get all the answers
levelDiffiController.get('/', async (req, res) => {
    try {
        let answers = await getRepository(LevelDifficulty).find({relations: ["difficulty"]})
        if (answers) {
            return res.json(answers)
        } return res.status(404).json({ message: 'no questions found' })
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

