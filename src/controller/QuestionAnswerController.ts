import { Router } from "express";
import { getRepository } from "typeorm";
import { Difficulty } from "../entity/Difficulty";



export const questionAnswerController = Router()
questionAnswerController.get('/:id', async (req, res)=>{
    try {
        const repo = getRepository(Difficulty);
        const difficulty = await repo.findOne( req.params.id, {relations: ["levelDifficulty"] })
        if(difficulty){
            return res.json(difficulty)
        }return res.status(404).json({ message: "Aucun article disponible" })
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

questionAnswerController.get('/', async (req, res)=>{
    try {

        const repo = getRepository(Difficulty);
        const difficulty= await repo.find({relations: ["levelDifficulty"]})

        if(difficulty){
            return res.json(difficulty)
        }return res.status(404).json({ message: "Aucun article disponible" })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


