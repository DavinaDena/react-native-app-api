import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import bcrypt from 'bcrypt'
import { generateToken } from "../utils/token";
import passport from "passport";



export const userRoute = Router()


// //to get/find user by id using token
// userRoute.get('/:id', passport.authenticate('jwt', {session: false}), async (req, res)=> {
//     //if you are an Admin or, your user ID corresponds to the params
//     if(req.user.role =='Admin' || req.user.id == Number(req.params.id)){
//         //we make a try catch
//         try {
//             //new variable named user that is going to try to find the user on the database
//             //based on the given parameters
//             let user = await getRepository(User).findOne(req.params.id);
//             //if we find the user and it correspons
//             if(user){
//                 //then we return the user
//                 return res.json(user)
//             }
//             //if the user doesnt exist, we send a error message
//             return res.status(404).json({ message: 'user doesnt exist'})

//         } catch (error) {
//             //if nothing of this works, we are going to receive an error
//             console.log(error);
//             res.status(400).json(error);
//         }
//     }
//     //if the params doesnt correspond to the user or we are not an admin
//     //we send an error message saying the client doesnt have the rights to acccess the info
//     return res.status(401).json({ message: "you dont have the necessary rights" })

// })


//user by id
userRoute.get('/:id', async (req, res) => {
    try {
        let user = await getRepository(User).findOne(req.params.id)
        if (user) {
            return res.json(user)
        }
        return res.status(404).json({ message: "User doesnt exist" })
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
})

//get the account/profil of user with their token
// userRoute.get('/account', async (req, res) => {
//     let user = await getRepository(User).findOne(req.user)
//     res.json(user)
// })


userRoute.post('/register', async (req, res) => {
    try {

        const newUser = new User;
        Object.assign(newUser, req.body);
        const exists = await getRepository(User).findOne({ email: newUser.email })

        if (exists) {
            res.status(401).json({ error: 'Email already used' })
            return;
        }

        newUser.role = 'user';
        newUser.password = await bcrypt.hash(newUser.password, 11)

        await getRepository(User).save(newUser)
        res.status(201).json({
            user: newUser,
            token: generateToken({
                email: newUser.email,
                name: newUser.name,
                id: newUser.id,
                role: newUser.role
            })
        });

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})


userRoute.post('/login', async (req, res) => {
    try {

        const user = await getRepository(User).findOne({ email: req.body.email })

        if (user) {
            let samePWD = await bcrypt.compare(req.body.password, user.password)
            if (samePWD) {
                return res.json({
                    user,
                    token: generateToken({
                        id: user.id,
                        email: user.email,
                        role: user.role
                    })
                })
            } res.status(401).json({ error: 'Wrong Password' })
            return;
        } res.status(404).json({ error: 'User doesnt exist' })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})



//to get all users
userRoute.get('/', async (req, res) => {
    try {
        let users = await getRepository(User).find()
        if (users) {
            return res.json(users)
        } return res.status(404).json({ message: 'no users' })
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
