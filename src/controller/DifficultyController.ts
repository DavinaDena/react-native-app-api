import { Router } from "express";
import { getRepository } from "typeorm";
import { Difficulty } from "../entity/Difficulty";

//WE NEED TO MAKE AN ADMIN PERMISSION TO ADD QUESTIONS!!


export const difficultyController = Router()


difficultyController.get('/:id', async (req, res)=>{
    try {
        
        let difficulties= await getRepository(Difficulty).findOne(req.body.id, {relations: ['levelDifficulty']})
        if(difficulties){
            return res.json(difficulties)
        }return res.status(404).json({ message: 'no questions found' })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

})


difficultyController.post('/', async (req, res)=>{
    try {
        const newDifficulty = new Difficulty();
        Object.assign(newDifficulty, req.body)

        await getRepository(Difficulty).save(newDifficulty)

        res.status(201).json(newDifficulty)

    } catch (error) {
        console.log(error);
        res.status(400).json(error)
    }
})

difficultyController.get('/', async (req, res)=>{
    try {
        
        let difficulties= await getRepository(Difficulty).find()
        if(difficulties){
            return res.json(difficulties)
        }return res.status(404).json({ message: 'no questions found' })

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

})

