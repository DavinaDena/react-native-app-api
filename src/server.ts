import express from 'express';
import cors from 'cors';
import { configurePassport } from './utils/token';
import { userRoute } from './controller/UserController';
import passport from "passport";
import { difficultyController } from './controller/DifficultyController';
import { levelDiffiController } from './controller/LevelDiffiController';
import { questionAnswerController } from './controller/QuestionAnswerController';

export const server = express();



server.use(express.json())
server.use(cors());

configurePassport()

server.use(express.static('public'))
server.use(passport.initialize());

server.use('/api/user', userRoute)
server.use('/api/difficulty', difficultyController)
server.use('/api/levelDifficulty', levelDiffiController)
server.use('/api/QA', questionAnswerController)
